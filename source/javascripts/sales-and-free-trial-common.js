(function() {
  var paramsFetcher = function () {
    //Looks for each of the params in the url, e.g.?key=value
    var regex = /[?&]([^=#]+)=([^&#]*)/g;
    var params = {};
    var match = '';

    while (match = regex.exec(window.location.href)) {
      params[match[1]] = match[2];
    }

    return (Object.keys(params).length > 0) ? params : null;
  };
  
  var paramsParser = function (params) {
    if (!params) return null;
    var validParams = {
      email: {},
    };

    Object.keys(params).forEach(function (param) {
      if (param.toLowerCase() === "email") {
        validParams.email = params[param];
      }
    });

    return validParams;
  }

  MktoForms2.loadForm("//app-ab13.marketo.com", "194-VVC-221", 1318, function (form) {
    form.onSuccess(function (values, followUpUrl) {

      form.getFormElem().hide();
      document.getElementById('confirmform').style.visibility = 'visible';

      dataLayer.push({ event: 'mktoLead', mktoFormId: form.getId() });
      return false;
    });
    function getgacid() {
      try {
        var tracker = ga.getAll()[0];
        return tracker.get('clientId');
      } catch (e) {
        return 'n/a';
      }
    }
    form.vals({
      'gacid': getgacid()
    });

    var urlParams = paramsFetcher();
    var parsedParams = paramsParser(urlParams);

    if (parsedParams) {
      $('input#Email').val(parsedParams.email);
    }
  });
})();
